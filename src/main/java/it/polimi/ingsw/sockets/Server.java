package it.polimi.ingsw.sockets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private int port;
	private ServerSocket serverSocket; 
	
	public Server(int port) {
		this.port = port; 	
	}
	
	public void startServer() {
		try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server ready");
            while (isStopped()) {
                Socket socket = serverSocket.accept();
                new ClientHandler(new SocketCommunicator(socket)).start();
            }
            serverSocket.close();
        } catch (IOException ex) {
            throw new AssertionError("Weird errors with I/O occured, please verify environment config", ex);
        }
	}
		

	private boolean isStopped() {
		return true;
	}

	public static void main(String[] args) { 
		Server server = new Server(1337);
        server.startServer();
	}

}
