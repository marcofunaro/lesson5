package it.polimi.ingsw.sockets;

public class ClientHandler extends Thread {
	
	Communicator client;
	public ClientHandler(Communicator c) {
		client = c;
	}
	
	@Override
	public void run(){
		String command = "";

		do {
            command = client.receive();
            client.send(command);
        } while(!command.equals("exit"));
        client.close();
    }

}
