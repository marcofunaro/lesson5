package it.polimi.ingsw.sockets;

public interface Communicator {

	void send(String msg);
	String receive();
	void close();
	
}
